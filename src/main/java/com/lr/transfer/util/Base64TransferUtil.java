package com.lr.transfer.util;

import org.springframework.util.Base64Utils;
import sun.misc.BASE64Decoder;

import java.io.*;

public class Base64TransferUtil {
//    public static void main(String[] args) {
//        String strImg = GetImageStr();
//     //   strImg.replaceAll("\n","");
//        System.out.println(strImg);
//
//        //GenerateImage(strImg);
//
//
//    }

    /**
     * 图片转化成base64字符串
     *
     * @param imgFile
     * @return
     */
    public static String GetImageStr(String imgFile) {//将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        // String imgFile = "C:\\Users\\18285\\Desktop\\身份证图片\\1111.jpg";//待处理的图片
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //对字节数组Base64编码
        // BASE64Encoder encoder = new BASE64Encoder();
        // return encoder.encode(data);//返回Base64编码过的字节数组字符串
        return Base64Utils.encodeToString(data);
    }


    public static String GetImageStr(InputStream in) {
        byte[] data = null;
        //读取图片字节数组
        try {
            data = new byte[in.available()];
            in.read(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Base64Utils.encodeToString(data);
    }


    /**
     * 对字节数组字符串进行Base64解码并生成图片
     *
     * @param imgStr
     * @param imgFilePath
     * @return
     */
    public static boolean GenerateImage(String imgStr, String imgFilePath) {
        //图像数据为空
        if (imgStr == null) {
            return false;
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            byte[] b = Base64Utils.decode(imgStr.getBytes());
            //Base64解码
            //byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                //调整异常数据
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            //生成jpeg图片
            //String imgFilePath = "C:\\Users\\18285\\Desktop\\new.jpg";//新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
