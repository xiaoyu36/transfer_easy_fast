package com.lr.transfer.controller;

import com.lr.transfer.component.ResourceComponent;
import com.lr.transfer.util.Base64TransferUtil;
import com.lr.transfer.util.TransferUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jodconverter.DocumentConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 用户控制器.
 */
@RestController
@Slf4j
public class TransferController {

    @GetMapping("/")
    public ModelAndView toTransfer(Model model) {
        model.addAttribute("title", "转换web");
        return new ModelAndView("transfer", "model", model);
    }

    @GetMapping("/lr/transfer")
    public ModelAndView transferGet(Model model) {
        model.addAttribute("title", "PDF/Word 转换");
        return new ModelAndView("transfer", "model", model);
    }

    /**
     * 方法描述:图片转换成Base64
     *
     * @author liran
     * @since 2018/9/29 13:25
     */
    @PostMapping(value = "/lr/transferImage")
    public ModelAndView transfer(Model model, MultipartFile file) throws Exception {
        String suffix = TransferUtil.getFileSuffix(file.getOriginalFilename());
        if (StringUtils.equals("jpg", suffix) || StringUtils.equals("jpeg", suffix)
                || StringUtils.equals("png", suffix) || StringUtils.equals("gif", suffix)) {
            model.addAttribute("isSuccess", "生成成功");
            String base64 = Base64TransferUtil.GetImageStr(file.getInputStream());
            model.addAttribute("base64", base64);
            model.addAttribute("originBase64", true);
            return new ModelAndView("success", "model", model);
        } else {
            throw new Exception("不支持此图片格式：" + suffix + "");
        }
    }

    /**
     * 转换PDF/WORD
     */
    @PostMapping(value = "/lr/transfer")
    public ModelAndView transfer(Model model, MultipartFile file, Integer type) throws Exception {
        model.addAttribute("originMD5", false);
        // 上传文件保存到磁盘
        String suffix = TransferUtil.getFileSuffix(file.getOriginalFilename());
        String webFile = TransferUtil.saveLocation(suffix, file);

        model.addAttribute("isSuccess", "【" + suffix + "】文件转换成功");
        if (type == 0 && (StringUtils.equalsIgnoreCase(TransferUtil.WORD_SUFFIX_DOC, suffix) || StringUtils.equalsIgnoreCase(TransferUtil.WORD_SUFFIX_DOCX, suffix))) {
            //word 转换pdf
            String newPdfWebPath = TransferUtil.transferWordToPdf(webFile);
            model.addAttribute("viewFilePath", newPdfWebPath);
        } else if (StringUtils.equalsIgnoreCase(TransferUtil.PDF_SUFFIX, suffix)) {
            //pdf 转换为img
            List<String> webImage = TransferUtil.transferPdfToWebImage(webFile);
            StringBuilder sb = new StringBuilder();
            webImage.forEach(s -> sb.append(s).append(","));
            model.addAttribute("viewFilePath", sb.toString());
        } else {
            model.addAttribute("isSuccess", "【" + suffix + "】文件类型不支持！");
        }
        return new ModelAndView("success", "model", model);
    }

}
